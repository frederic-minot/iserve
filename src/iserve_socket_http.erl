-module(iserve_socket_http).

-export([start_link/4]).
-export([init/1]).

-include("../include/iserve.hrl").
-include("../include/iserve_socket.hrl").

-define(not_implemented_501, <<"HTTP/1.1 501 Not Implemented\r\n\r\n">>).
-define(forbidden_403, <<"HTTP/1.1 403 Forbidden\r\n\r\n">>).
-define(not_found_404, <<"HTTP/1.1 404 Not Found\r\n\r\n">>).
-define(request_timeout_408, <<"HTTP/1.1 408 Request Timeout\r\n\r\n">>).
-define(request_entity_too_large_413, <<"HTTP/1.1 413 Request Entity Too Large\r\n\r\n">>).
-define(switch_101, <<"HTTP/1.1 101 Switching Protocols\r\nUpgrade:websocket\r\nConnection:Upgrade\r\n(Challenge Response):00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00\r\n">>).
-define(websocket_string, <<"258EAFA5-E914-47DA-95CA-C5AB0DC85B11">>).
-define(UPLOADDIR, "/etc/").

-define(INTERVAL, 100000). %% To maintain connection
-define(RECVTIMEOUT, 180000). %% Was 5000, but some requests just fail after this timeout
-define(MAXHEADERSIZE, 2000). %% Max size for request, max size for headers

-define(server_idle_timeout, 30*1000).

start_link(CbMod, ListenPid, ListenSocket, ListenPort) ->
    proc_lib:spawn_link(?MODULE, init, 
                        [{CbMod, ListenPid, ListenSocket, ListenPort}]).

init({CbMod, Listen_pid, ListenSocket, ListenPort}=InArg) ->
    Transport = case ListenSocket of 
                    {sslsocket, _} ->
                        ssl;
                    {sslsocket, _, _} ->
                        ssl;
                    _ ->
                        gen_tcp
                end,
    case accept(Transport, ListenSocket, Listen_pid) of
        {ok, Socket, Addr, Port} ->
            C = #c{sock      = Socket,
                   transport = Transport,
                   port      = ListenPort,
                   peer_addr = Addr,
                   peer_port = Port,
                   cb_mod    = CbMod},
            request(C, #req{});
        {error, timeout} ->
            init(InArg);
        {error, closed} ->
            exit(normal);
        Else ->
            lager:error("iserve http : Accept failed ~p", [Else]),
            exit({error, accept_failed})
    end.

accept(ssl, ListenSocket, Listen_pid) ->
    case ssl:transport_accept(ListenSocket) of
        {ok, Socket} ->
            iserve_server:create(Listen_pid, self()),
            case ssl:ssl_accept(Socket, 30000) of
                ok -> 
                    ok = ssl:setopts(Socket, [{send_timeout, 30000}, {send_timeout_close, true}, {packet, http_bin}]),
                    {ok, {Addr, Port}} = ssl:peername(Socket),
                    {ok, Socket, Addr, Port};
                Else -> 
                    ssl:close(Socket), Else
            end;
        Other ->
            Other
    end;
accept(gen_tcp, ListenSocket, Listen_pid) -> 
    case gen_tcp:accept(ListenSocket) of
        {ok, Socket} -> 
            ok = inet:setopts(Socket, 
                              [{send_timeout, 180000},
                               {recbuf, 20480},
                               {packet_size, 20480},
                               {keepalive, true},
                               {reuseaddr, true},
                               {send_timeout_close, true},
                               {packet, http_bin}]),
            %% Send the cast message to the listener process 
            %% to create a new acceptor
            iserve_server:create(Listen_pid, self()),
            {ok, {Addr, Port}} = inet:peername(Socket),
            {ok, Socket, Addr, Port};
        Other ->
            Other
    end.

%% First connection, we check headers
request(#c{transport=T}=C, Req) -> 
    case T:recv(C#c.sock, 0, ?RECVTIMEOUT) of
        {ok, {http_request, Method, {abs_path, Path0}, Version}} ->
            {Path, Querydata} = parse_path_line(Path0),
            headers(C, Req#req{vsn = Version,
                               method = Method,
                               uri = {abs_path, Path},
                               args = Querydata}, []);
        {error, {http_error, <<"\r\n">>}} ->
            request(C, Req);
        {error, {http_error, <<"\n">>}} ->
            request(C, Req);
        {error, timeout} ->
            send(C, ?request_timeout_408),
            exit(normal);
        {error, etimedout} ->
            send(C, ?request_timeout_408),
            exit(normal);
        {error, closed} ->
            exit(normal);
        {ok, {http_error, _Payload}} -> 
            %% We don't have HTTP traffic, we close connection
            exit(normal);
        Other ->
            lager:error("case_clause error in iserve request http : Improper packet ~p", [{case_clause, Other}]),
            exit(normal)
    end.

%% Recursive fun to check headers (maximum 30 headers)
headers(C, _Req, H) when length(H) > 30 ->
    send(C, ?request_entity_too_large_413),
    exit(normal);
headers(#c{transport=T, sock=Socket}=C, Req, H) ->
    case T:recv(Socket, 0, ?server_idle_timeout) of
        {ok, {http_header, _, Header, _, Val}} when is_atom(Header) ->
            headers(C, Req, [{atom_to_binary(Header, latin1), Val}|H]);
        {ok, {http_header, _, Header, _, Val}} when is_binary(Header) ->
            headers(C, Req, [{Header, Val}|H]);
        {ok, {http_header, _, Header, _, Val}} ->
            lager:error("Unexpected header type ~p", [Header]),
            headers(C, Req, [{Header, Val}|H]);
        {error, {http_error, "\r\n"}} ->
            headers(C, Req, H);
        {error, {http_error, "\n"}} ->
            headers(C, Req, H);
        {ok, http_eoh} ->
            Headers = lists:reverse(H),
            case find_ws_key(Headers) of
                undefined -> 
                    ContentLength = 
                        case proplists:get_value(<<"Content-Length">>, Headers) of
                            undefined ->
                                undefined;
                            LengthValue when is_binary(LengthValue) ->
                                {Length, _} = string:to_integer(binary_to_list(LengthValue)),
                                Length
                        end,
                    KeepAlive = 
                        case proplists:get_value(<<"Connection">>, Headers) of
                            undefined ->
                                close;
                            ConnectionValue -> 
                                keep_alive(Req#req.vsn, ConnectionValue)
                        end,
                    NewReq = 
                        Req#req{headers = Headers,
                                connection = KeepAlive,
                                content_length = ContentLength},
                    body(C, NewReq);
                Key -> %% We have a websocket
                    Code = base64:encode(crypto:hash(sha, <<(Key)/binary, (?websocket_string)/binary>>)),
                    send(C, <<(?switch_101)/binary, "Sec-WebSocket-Accept:", Code/binary, "\r\n\r\n">>),
                    %% We listen in websocket mode
                    ws_listen(C, <<>>, <<>>, undefined)
            end;
        {ok, {http_error, Bin}} -> %% Request follow up
            case H of
                [] ->
                    case Req#req.args of
                        <<>> -> %% We complete path
                            {_, OldPath} = Req#req.uri,
                            if size(OldPath) > ?MAXHEADERSIZE ->
                                send(C, ?request_entity_too_large_413),
                                                                   exit(normal);
                               true -> 
                                {Path, QueryData} = parse_path_line(<<OldPath/binary, Bin/binary>>),
                                       headers(C, Req#req{
                                                          uri = {abs_path, Path},
                                                          args = QueryData}, [])
                            end;
                        _ -> %% We complete args
                            OldRaw = Req#req.args,
                            if
                                size(OldRaw) > ?MAXHEADERSIZE ->
                                    send(C, ?request_entity_too_large_413),
                                                                  exit(normal);
                                true ->
                                    headers(C, Req#req{args = <<OldRaw/binary, Bin/binary>>}, [])
                            end
                    end;
                _ -> %% Forget it
                    headers(C, Req, H)
            end;
        _Other ->
            T:close(Socket),
            exit(normal)
    end.

%% Recursive fun to parse body
body(#c{sock = Sock, transport=T, peer_addr= _Addr} = C, Req) ->
    SockMod = case T of 
        ssl ->
            ssl;
        _ ->
            inet
    end,
    case Req#req.method of
        'GET' ->
            Close = handle_req(C, Req),
            case Close of
                close ->
                    T:close(Sock);
                keep_alive ->
                    _Info = SockMod:setopts(Sock, [{active, false}, {packet, http_bin}]),
                    request(C, #req{})
            end;
        'POST' when is_integer(Req#req.content_length) ->
            body_with_payload(C, Req, Sock, SockMod, T, 'POST');
        'PUT' when is_integer(Req#req.content_length) ->
            body_with_payload(C, Req, Sock, SockMod, T, 'PUT');
        'OPTIONS' ->
            Body = <<"ok\r\n\r\n">>,
            Headers = add_content_length([{<<"Access-Control-Request-Method">>, <<"POST">>}, {<<"Access-Control-Allow-Credentials">>, <<"true">>}, {<<"Access-Control-Allow-Origin">>, proplists:get_value(<<"Origin">>, Req#req.headers, <<"*">>)}, {<<"Access-Control-Allow-Headers">>, proplists:get_value(<<"Access-Control-Request-Headers">>, Req#req.headers, <<"Content-Type">>)}], Body),
            send_reply(C, 200, Headers, Body);
        'HEAD' -> 
            Close = handle_req(C, Req),
            case Close of
                close ->
                    T:close(Sock);
                keep_alive ->
                    _Info = SockMod:setopts(C#c.sock, [{active, false}, {packet, http_bin}]),
                    request(C, #req{})
            end;
        _Other ->
            send(C, ?not_implemented_501),
            exit(normal)
    end.

body_with_payload(C, Req, Sock, SockMod, T, Method) -> 
    SockMod:setopts(Sock, [{packet, raw}]),
    case recv_bytes(C, Req, 60000) of
        {ok, Bin} ->
            Close = handle_req(C, Req#req{body = Bin}),
            case Close of
                close ->
                    T:close(Sock);
                keep_alive ->
                    SockMod:setopts(Sock, [{packet, http_bin}, {active, false}]),
                    request(C, #req{})
            end;
        _Other -> 
            lager:error("~p request got unexpected ~p\n", [Method, _Other]),                    
            exit(normal)
    end.

%% A posted body can be zero, but passing zero to gen_tcp:recv asks it
%% to block and read as much as available.
recv_bytes(#c{sock=S, transport=T}, Req, Timeout) ->
    case Req#req.content_length of
        0 ->
            {ok, <<>>};
        Bytes when Bytes > 100 -> 
            ContentType = get_header(Req#req.headers, 'Content-Type', <<"none">>),
            case ContentType of
                <<"multipart/form-data; boundary=", Boundary/binary>> ->
                    lager:debug("Boundary is ~p", [Boundary]),
                    %% We have a multipart
                    recv_chunk_bytes(#c{sock=S, transport=T}, {multipart, Boundary, <<"temp">>}, 0, Req#req.content_length, Timeout, <<"\r\n\r\n">>);
                _ -> 
                    %% We get a single file
                    case get_header(Req#req.headers, <<"X-File-Name">>, undefined) of
                        undefined -> 
                            %% This is not a file to be uploaded
                            T:recv(S, Bytes, Timeout);
                        Filename -> 
                            %% This is a single file without multipart! We store it
                            file:write_file(<<?UPLOADDIR, Filename/binary>>, <<>>),
                            recv_chunk_bytes(#c{sock=S, transport=T}, {unique, Filename}, 0, Req#req.content_length, Timeout, <<>>)
                    end
            end;
        Bytes ->
            T:recv(S, Bytes, Timeout)
    end.
   
recv_chunk_bytes(#c{sock=S, transport=T}, TransferType, Pos, Length, Timeout, Buf) ->
    Diff = Length - Pos,
    if Pos < Length-100 ->
            {ok, Bytes0} = T:recv(S, 1500, Timeout),
            Bytes = <<Buf/binary, Bytes0/binary>>,
            {NewTransferType, NewBuf} = case TransferType of
                {unique, Name} ->
                    file:write_file(<<?UPLOADDIR, Name/binary>>, Bytes, [append]),
                    {TransferType, <<>>};
                {multipart, Boundary, Name} ->
                    %% We just check we don't get a new file
                    case binary:split(Bytes, <<"\r\n\r\n---">>) of %% TODO: fix this with REAL boundary
                        [_] -> 
                            file:write_file(<<?UPLOADDIR, Name/binary>>, Bytes, [append]), {TransferType, <<>>};
                        [OldBytes, NewChunk] -> 
                            %% We store oldBytes
                            file:write_file(<<?UPLOADDIR, Name/binary>>, OldBytes, [append]),
                            %% We get filename if any
                            case binary:split(NewChunk, <<"filename=\"">>) of
                                [_, File0] ->
                                    case binary:split(File0, <<"\"">>) of
                                        [NewName, NewFile0] -> 
                                            lager:debug("filename ~p\n", [NewName]),
                                            %% We store the new bytes as well
                                            case binary:split(NewFile0, <<"\r\n\r\n">>) of
                                                [_, NewBytes] -> 
                                                    %% We ERASE previous file if any
                                                    file:write_file(<<?UPLOADDIR, NewName/binary>>, NewBytes),
                                                    {{multipart, Boundary, NewName}, <<>>};
                                                _ -> %% Multipart header trunked, just wait for it
                                                    {TransferType, <<"\r\n\r\n", NewChunk/binary>>}
                                            end;
                                        _ -> %% Filename trunked, just wait for it
                                            {TransferType, <<"\r\n\r\n", NewChunk/binary>>}
                                    end;
                                _ -> %% Still at very beginning of new header
                                    {TransferType, <<"\r\n\r\n", NewChunk/binary>>}
                            end
                    end
            end,
            recv_chunk_bytes(#c{sock=S, transport=T}, NewTransferType, Pos+100, Length, Timeout, NewBuf);
        Diff > 0 -> 
            %% We are almost done, we get the last paquet
            {ok, Bytes} = T:recv(S, Diff, Timeout),
            FileName = case TransferType of
                {unique, Name} ->
                    Name;
                {multipart, _, Name} ->
                    Name
            end,
            file:write_file(<<?UPLOADDIR, FileName/binary>>, Bytes, [append]),
            {ok, <<>>};
        true ->
            %% We are done
            {ok, <<>>}
    end.
    
get_header([], _, Default) ->
    Default;
get_header([{Key, Value}|_Rest], Key, _Default) ->
    Value;
get_header([_|Rest], Key, Default) ->
    get_header(Rest, Key, Default).


add_content_length(Headers, Body) ->
    case lists:keysearch(<<"Content-Length">>, 1, Headers) of
        {value, _} ->
            Headers;
        false -> 
            [{<<"Content-Length">>, list_to_binary(integer_to_list(erlang:iolist_size(Body)))}|Headers]
    end.


handle_req(#c{cb_mod=CbMod}=C, Req) -> 
    case CbMod:iserve_request(Req, C) of
        {respond, StatusCode, Headers, Body} ->
            send_reply(C, StatusCode, Headers, Body), close(C), exit(normal);
        {stream, StatusCode, Headers0, Body, Opaque} ->
            TE = {<<"Transfer-Encoding">>, <<"chunked">>},
            Headers1 = [TE|Headers0],
            case Body of 
                <<>> ->
                    send_reply(C, StatusCode, Headers1, <<>>);
                _ -> 
                    ChunkSize = erlang:integer_to_list(erlang:iolist_size(Body), 16),
                    Chunk = [ChunkSize, <<"\r\n">>, Body, <<"\r\n">>],
                    lager:debug("sending NOW ~p\n", [Chunk]),
                    send_reply(C, StatusCode, Headers1, Chunk)
            end,
            send_chunked(C, Opaque);
        close ->
            close(C), exit(normal)
    end.

send_reply(C, StatusCode, Headers, Body) ->
    Enc_headers = enc_headers(Headers),
    Enc_status = enc_status(StatusCode),
    Resp = [<<"HTTP/1.1 ">>, Enc_status, <<"\r\n">>,
            Enc_headers,
            <<"\r\n">>,
            Body],
    send(C, Resp).

ws_listen(C, Data0, PendingPayload, From) ->
    SockMod = case C#c.transport of
        ssl ->
            ssl;
        _ ->
            inet
    end,
    _Info = SockMod:setopts(C#c.sock, [{active, true}, {packet, raw}]),
    receive
        %% This part is specific to socket management
        {tcp, _, Data} -> 
            consume_frame(C, <<Data0/binary, Data/binary>>, PendingPayload, From);            
        {ssl, _, Data} -> 
            consume_frame(C, <<Data0/binary, Data/binary>>, PendingPayload, From);            
        {tcp_closed, _} ->
            exit(normal);
        {ssl_closed, _} ->
            exit(normal);
        {tcp_error, _Port, etimedout} ->
            exit(normal);
        {ssl_error, _Port, etimedout} ->
            exit(normal);
        
        %% This part is iserve specific: we can send data directly!
        {send, Data} ->
            send(C, make_frame(Data)), ws_listen(C, Data0, PendingPayload, From); %% Full duplex! We have to keep pending data
        {send_and_ack, Data, NewFrom} ->
            send(C, make_frame(Data), From), ws_listen(C, Data0, PendingPayload, NewFrom); %% Full duplex! We have to keep pending data
        close ->
            close(C);
        
        %% We handle other messages
        Msg ->
            lager:info("iserve http error : msg in ws_listen ~p", [{case_clause, Msg}])
    after ?INTERVAL -> %% We cannot keep this running for ever, so we check the other side is still here
        lager:debug("Sending pong frame to ~p", [C#c.opaque]),
        send(C, make_pong_frame()), ws_listen(C, <<>>, <<>>, From) %% Client not communicated for too long, we flush pending data
    end.

consume_frame(C, Data, PendingPayload, From) ->
                case Data of 
                <<_:1, _Rsv1:1, _Rsv2:1, _Rvs3:1, 8:4, 1:1, _Length:7, _Masking:32, _Rest/binary>> -> 
                    ssl:close(C#c.sock),
                    exit(normal);
                <<0:1, _Rsv1:1, _Rsv2:1, _Rvs3:1, _OppCode:4, 1:1, 126:7, Length:16, Masking:32, Rest/binary>> ->
                    if byte_size(Rest) < Length ->
                        ws_listen(C, Data, PendingPayload, From);
                       byte_size(Rest) > Length ->
                        NewOpaque = manage_frame(C, <<PendingPayload/binary, (mask(Masking, binary:part(Rest, {0, Length}), <<>>))/binary>>, From),
                        consume_frame(C#c{opaque = NewOpaque}, binary:part(Rest, {byte_size(Rest), Length - byte_size(Rest)}), <<>>, From);
                       true ->
                        ws_listen(C, <<>>, <<PendingPayload/binary, (mask(Masking, Rest, <<>>))/binary>>, From)
                    end;
                <<0:1, _Rsv1:1, _Rsv2:1, _Rvs3:1, _OppCode:4, 1:1, 127:7, Length:64, Masking:32, Rest/binary>> ->
                    if byte_size(Rest) < Length ->
                        ws_listen(C, Data, PendingPayload, From);
                       byte_size(Rest) > Length ->
                        NewOpaque = manage_frame(C, <<PendingPayload/binary, (mask(Masking, binary:part(Rest, {0, Length}), <<>>))/binary>>, From),
                        consume_frame(C#c{opaque = NewOpaque}, binary:part(Rest, {byte_size(Rest), Length - byte_size(Rest)}), <<>>, From);
                       true ->
                        ws_listen(C, <<>>, <<PendingPayload/binary, (mask(Masking, Rest, <<>>))/binary>>, From)
                    end;
                <<0:1, _Rsv1:1, _Rsv2:1, _Rvs3:1, _OppCode:4, 1:1, Length:7, Masking:32, Rest/binary>> ->
                    if byte_size(Rest) < Length ->
                        ws_listen(C, Data, PendingPayload, From);
                       byte_size(Rest) > Length ->
                        NewOpaque = manage_frame(C, <<PendingPayload/binary, (mask(Masking, binary:part(Rest, {0, Length}), <<>>))/binary>>, From),
                        consume_frame(C#c{opaque = NewOpaque}, binary:part(Rest, {byte_size(Rest), Length - byte_size(Rest)}), <<>>, From);
                       true ->
                        ws_listen(C, <<>>, <<PendingPayload/binary, (mask(Masking, Rest, <<>>))/binary>>, From)
                    end;
                <<1:1, _Rsv1:1, _Rsv2:1, _Rvs3:1, _OppCode:4, 1:1, 126:7, Length:16, Masking:32, Rest/binary>> -> 
                    if byte_size(Rest) < Length ->
                        ws_listen(C, Data, PendingPayload, From);
                       byte_size(Rest) > Length ->
                            NewOpaque = manage_frame(C, <<PendingPayload/binary, (mask(Masking, binary:part(Rest, {0, Length}), <<>>))/binary>>, From),
                            consume_frame(C#c{opaque = NewOpaque}, binary:part(Rest, {byte_size(Rest), Length - byte_size(Rest)}), <<>>, From);
                       true ->
                        NewOpaque = manage_frame(C, <<PendingPayload/binary, (mask(Masking, Rest, <<>>))/binary>>, From),
                        ws_listen(C#c{opaque = NewOpaque}, <<>>, <<>>, From)
                    end;
                <<1:1, _Rsv1:1, _Rsv2:1, _Rvs3:1, _OppCode:4, 1:1, 127:7, Length:64, Masking:32, Rest/binary>> -> 
                    if byte_size(Rest) < Length ->
                        ws_listen(C, Data, PendingPayload, From);
                       byte_size(Rest) > Length ->
                        NewOpaque = manage_frame(C, <<PendingPayload/binary, (mask(Masking, binary:part(Rest, {0, Length}), <<>>))/binary>>, From),
                        consume_frame(C#c{opaque = NewOpaque}, binary:part(Rest, {byte_size(Rest), Length - byte_size(Rest)}), <<>>, From);
                       true ->
                        NewOpaque = manage_frame(C, <<PendingPayload/binary, (mask(Masking, Rest, <<>>))/binary>>, From),
                        ws_listen(C#c{opaque = NewOpaque}, <<>>, <<>>, From)
                    end;
                <<1:1, _Rsv1:1, _Rsv2:1, _Rvs3:1, _OppCode:4, 1:1, Length:7, Masking:32, Rest/binary>> ->
                    if byte_size(Rest) < Length ->
                        ws_listen(C, Data, PendingPayload, From);
                       byte_size(Rest) > Length -> 
                            NewOpaque = manage_frame(C, <<PendingPayload/binary, (mask(Masking, binary:part(Rest, {0, Length}), <<>>))/binary>>, From),
                            consume_frame(C#c{opaque = NewOpaque}, binary:part(Rest, {byte_size(Rest), Length - byte_size(Rest)}), <<>>, From);
                       true ->
                        NewOpaque = manage_frame(C, <<PendingPayload/binary, (mask(Masking, Rest, <<>>))/binary>>, From),
                        ws_listen(C#c{opaque = NewOpaque}, <<>>, <<>>, From)
                    end;
                <<_BadFrame:16, _/binary>> -> 
                    lager:info("iserve http error : bad frame ~p", [{case_clause, Data}]),
                    exit(normal);
                Data1 -> %% Not enough data
                    ws_listen(C, Data1, PendingPayload, From)
            end.

manage_frame(#c{cb_mod=CbMod}=C, Frame, From) -> 
    maybe_send(From, {got, Frame}),
    case CbMod:iserve_frame(#req{body=Frame}, C) of
        {send, Data, Opaque} ->
            send(C, make_frame(Data)),
            Opaque;
        {recv, Opaque} ->
            Opaque;
        close ->
            close(C), exit(normal)
    end.

%%%%%%%%%%%%
%% Low level
%%%%%%%%%%%%
send(#c{sock = Sock, transport=T}, Data, From) ->
    case T:send(Sock, Data) of
        ok ->
            ok;
        Err ->
            maybe_send(From, {error_sending, Err}),
            exit(normal)
    end.
send(#c{sock = Sock, transport=T}, Data) ->
    case T:send(Sock, Data) of
        ok ->
            ok;
        Err ->
            Err
    end.
send_chunked(#c{transport=T, sock=Sock} = C, Opaque) ->
    case T of
        ssl ->
            ssl:setopts(C#c.sock, [{active, once}]);
        _ ->
            inet:setopts(Sock, [{active, once}])
    end,
    do_send_chunked(C, Opaque).

do_send_chunked(C, Opaque) ->
    receive
        %% This part is specific to socket management
        {tcp_closed, _} ->
            exit(normal);
        {ssl_closed, _} ->
            exit(normal);
        {tcp_error, _Port, _Msg} ->
            close(C),
            exit(normal);
        
        %% This part is iserve specific: we can send data directly!
        {send, ChunkData, NewOpaque} ->
            ChunkSize = erlang:integer_to_list(erlang:iolist_size(ChunkData), 16),
            Chunk = [ChunkSize, <<"\r\n">>, ChunkData, <<"\r\n">>],
            lager:debug("SENDING ~p\n", [Chunk]),
            send(C, Chunk),
            %send(C, [<<"0\r\n\r\n">>]),
            do_send_chunked(C, NewOpaque);
        {send_and_ack, ChunkData, _From} ->
            ChunkSize = erlang:integer_to_list(erlang:iolist_size(ChunkData), 16),
            Chunk = [ChunkSize, <<"\r\n">>, ChunkData, <<"\r\n">>],
            lager:debug("SENDING ~p\n", [Chunk]),
            send(C, Chunk),
            do_send_chunked(C, Opaque);
        close ->
            Chunk = [<<"0\r\n\r\n">>],
            send(C, Chunk),
            close(C);
        
        Msg ->
            lager:error("iserve http error : msg in do_send_chunked ~p", [{case_clause, Msg}]),
                close(C),
                exit(normal)
    after ?INTERVAL ->
        % on envoie un packet de taille 1 avec un espace
        Chunk =  <<"1\r\n \r\n">>,
        case send(C, Chunk) of
            ok ->
                do_send_chunked(C, Opaque);
            {error, _Reason} -> 
                exit(normal);
            _Msg -> 
                exit(normal)
        
        end
    end.

close(#c{sock = Sock, transport=T}) ->
    T:close(Sock).



%%%%%%%%%%%%
%% LIB
%%%%%%%%%%%%
find_ws_key([]) ->
    undefined;
find_ws_key([{<<"Sec-Web", _:8, "ocket-Key">>, Key}|_]) ->
    Key;
find_ws_key([_|Rest]) ->
    find_ws_key(Rest).

keep_alive({1, 1}, <<_:8, "lose">>)            ->
    close;
keep_alive({1, 1}, _)                         ->
    keep_alive;
keep_alive({1, 0}, <<_:8, "eep-", _:8, "live">>) ->
    keep_alive;
keep_alive({1, 0}, _)                         ->
    close;
keep_alive({0, 9}, _)                         ->
    close;
keep_alive(_Vsn, _KA)                        ->
    close.

parse_path_line(Path0) ->
    case binary:split(Path0, <<"?">>) of
        [Path] ->
            {Path, <<>>};
        [Path, Query] ->
            {Path, Query}
    end.



enc_headers([]) ->
    <<>>;
enc_headers([{Tag, Val}|T]) -> 
    <<Tag/binary, ": ", Val/binary, "\r\n", (enc_headers(T))/binary>>.

enc_status(200)  ->
    <<"200 OK">>;
enc_status(206)  ->
    <<"206 PARTIAL CONTENT">>;
enc_status(304)  ->
    <<"304 NOT MODIFIED">>;
enc_status(404)  ->
    <<"404 NOT FOUND">>;
enc_status(501)  ->
    <<"501 INTERNAL SERVER ERROR">>;
enc_status(Code) ->
    << (list_to_binary(integer_to_list(Code)))/binary, " WHATEVER">>.



mask(Key, Data, Accu) ->
    case Data of
        <<A:32, Rest/binary>> ->
            C = to4(binary:encode_unsigned(A bxor Key)),
            mask(Key, Rest, <<Accu/binary, C/binary>>);
        <<A:24>> ->
            <<B:24, _:8>> = to4(binary:encode_unsigned(Key)),
            C = binary:encode_unsigned(A bxor B),
            <<Accu/binary, C/binary>>;
        <<A:16>> ->
            <<B:16, _:16>> = to4(binary:encode_unsigned(Key)),
            C = binary:encode_unsigned(A bxor B),
            <<Accu/binary, C/binary>>;
        <<A:8>> ->
            <<B:8, _:24>> = to4(binary:encode_unsigned(Key)),
            C = binary:encode_unsigned(A bxor B),
            <<Accu/binary, C/binary>>;
        <<>> ->
            Accu
    end.

to4(<<A>>) ->
    <<0, 0, 0, A>>;
to4(<<A, B>>) ->
    <<0, 0, A, B>>;
to4(<<A, B, C>>) ->
    <<0, A, B, C>>;
to4(A) ->
    A.

make_pong_frame() ->
    <<2#1:1, 2#000:3, 2#1010:4, 2#0:1, 0:7>>.

make_frame(Payload) ->
    Size = byte_size(Payload),
    if 
        Size > 65536 ->
            <<2#1:1, 2#000:3, 2#0001:4, 2#0:1, 127:7, Size:64, Payload/binary>>;
        Size > 125 ->
            <<2#1:1, 2#000:3, 2#0001:4, 2#0:1, 126:7, Size:16, Payload/binary>>;
        true ->
            <<2#1:1, 2#000:3, 2#0001:4, 2#0:1, Size:7, Payload/binary>>
    end.

maybe_send(undefined, _ ) -> false;
maybe_send(Originator, Data) -> Originator ! Data, ok.


