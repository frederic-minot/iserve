%% @author fredericminot
%% @doc Supervisor to imap_voicemail

-module(iserve_server).

-behaviour(gen_server).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2,
		 code_change/3]).

-export([
		 start_link/1, %% Called from outside applications supervisors
		 create/2 %% Called from inside iserve, when an acceptor got a job
		]).

-record(state, {
				listen_socket,
				port,
				ip,
				acceptor,
				cb_mod,
				type
			   }).

-include("../include/iserve.hrl").

%%% API

start_link({PortAndIP, Type, Callback}) ->
	Name = list_to_atom(lists:concat([iserve_, port_of(PortAndIP)])),
	gen_server:start_link({local, Name}, ?MODULE, 
						  [PortAndIP, Type, Callback], []).

%% Send message to cause a new acceptor to be created
create(ServerPid, Pid) ->
	gen_server:cast(ServerPid, {create, Pid}).


%% Internal functions....

port_of({Port, _IP}) -> 
	Port;
port_of(Port)        -> 
	Port.

%% TODO: Not very ipv6 safe, uh.
ip_of({_Port, IP}) -> 
	IP;
ip_of(_Port)       -> 
	{0, 0, 0, 0}.

init([PortAndIP, Type, CbMod]) ->
	process_flag(trap_exit, true),
	Port = port_of(PortAndIP),
	IP = ip_of(PortAndIP),
	case Type of 
		{_, Certfile, Keyfile} -> 
			case ssl:listen(Port, [binary, {certfile, Certfile}, {keyfile, Keyfile}, {reuseaddr, true}, {reuse_sessions, true}, {ip, IP}, {active, false}, {backlog, 100}]) of
					{ok, ListenSocket} ->
						init_step2(ListenSocket, Type, Port, IP, CbMod);
					{error, Reason} -> 
						lager:error("iserve failed to start listening ~p", [{config, {PortAndIP}}, {resaon, Reason}]),
						{stop, Reason}
				end;
		_ -> 
			case gen_tcp:listen(Port, [binary, {reuseaddr, true}, {ip, IP}, {active, false}, {backlog, 100}]) of
				 {ok, ListenSocket} ->
					 init_step2(ListenSocket, Type, Port, IP, CbMod);
				 {error, Reason} -> 
						lager:error("iserve failed to start listening ~p", [{config, {PortAndIP}}, {resaon, Reason}]),
									{stop, Reason}
			 end
	end.
init_step2(ListenSocket, Type, Port, IP, CbMod) ->
	%%Create first accepting process
	Pid = start_iserve_socket(Type, ListenSocket, CbMod, Port),
	% check if we have to start an http connection manager
	{ok, #state{listen_socket = ListenSocket,
				type = Type,
				ip = IP,
				port = Port,
				acceptor = Pid,
				cb_mod = CbMod}}.

%% We don't do anything from calls yet
handle_call(_Request, _From, State) ->
	Reply = ok,
	{reply, Reply, State}.


%% Called by gen_server framework when the cast message from create/2 is received
handle_cast({create, _Pid}, #state{cb_mod = CbMod, listen_socket = ListenSocket, type = Type, port = Port} = State) ->
    New_pid = start_iserve_socket(Type, ListenSocket, CbMod, Port),
    {noreply, State#state{acceptor=New_pid}};
handle_cast(_Msg, State) ->
	{noreply, State}.

%% The current acceptor has died, wait a little and try again
handle_info({'EXIT', Pid, Abnormal}, #state{acceptor=Pid} = State) ->
	lager:error("iserve acceptor crashed ~p", [{Abnormal, State}]),
	terminate(<<>>, State), %% To release listening sockets
	timer:sleep(2000),
	New_Pid = start_iserve_socket(State#state.type,
								  State#state.cb_mod, 
								  State#state.listen_socket, 
								  State#state.port),
	{noreply, State#state{acceptor=New_Pid}};
%% We don't react to other info messages
handle_info(_Info, State) ->
	{noreply, State}.


terminate(_Reason, #state{type = _Type, listen_socket = Sock}) ->
    if
        is_tuple(Sock) ->
        	ssl:close(Sock);
        true ->
        	gen_tcp:close(Sock)
    end,
	ok.

code_change(_OldVsn, State, _Extra) ->
	{ok, State}.

% find the type of the server. http | raw | https are currently the type supported
start_iserve_socket(Type, ListenSocket, CbMod, Port) -> 
	case Type of 
		raw ->
			iserve_socket_raw:start_link(CbMod, self(), ListenSocket, Port);
		_ ->
			iserve_socket_http:start_link(CbMod, self(), ListenSocket, Port) %% Default is http
	end.
