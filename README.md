# iserve

### Overview
iserve is an Erlang library that provides a flexible TCP server to your application, with a stream oriented API.

Credits for the root concepts go to [Erlang Central](https://erlangcentral.org/wiki/index.php?title=A_fast_web_server_demonstrating_some_undocumented_Erlang_features)

It can be used as a lightweight web server.



### Getting started
#### Start a TCP listener from your supervisor
To use iserve in your application, you need to define it as a rebar dep or have
some other way of including it in Erlang's path.
You can then use the library by adding the following in a supervisor:

```
{iserve_server, start_link, [{80, {iserve_http, [{http}, {upload, Opaque}]}}]}
```

This will start a TCP server, listening on port `80`, calling back `iserve_http` for every new event, with `[{http}, upload]` as opaque arguments to serve HTTP and call upload module when something happens.

Alternatively, you can launch HTTPS by using ```
[{https, "/priv/server.cert", "/priv/server.key"}, {upload, Opaque}]}```
 instead of ```[{http}, {upload, Opaque}]```
