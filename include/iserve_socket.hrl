-ifndef(_ISERVE_SOCKET_).
-define(_ISERVE_SOCKET_, true).

-record(c, {
     sock,
     transport = gen_tcp,
     port,
     peer_addr,
     peer_port,
     cb_mod, % callback module M:iserve_request(#req{})
     opaque  % Opaque
    }).

-endif.
